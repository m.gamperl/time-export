$action = New-ScheduledTaskAction -Execute 'node' -WorkingDirectory $PSScriptRoot -Argument '.\src\index.js'
$trigger =  New-ScheduledTaskTrigger -AtLogOn

Register-ScheduledTask -Action $action -Trigger $trigger -TaskName "time-export" -Description "Exports Timesheet on every logon"