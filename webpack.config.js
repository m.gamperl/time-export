var webpack = require("webpack");
var path = require("path");
var fs = require('fs');

var nodeModules = {};
fs.readdirSync('node_modules')
  .filter(function(x) {
    return ['.bin'].indexOf(x) === -1;
  })
  .forEach(function(mod) {
    nodeModules[mod] = 'commonjs ' + mod;
  });

module.exports = {
  entry: "./src/index.js",
  output: {
      filename: "index.js",
      path: './dist'
  },
  target : "node",
  externals: nodeModules,
  module : {
    loaders  : [
      {
        test: /\.js$/,
        // "include" is commonly used to match the directories
        include: [
          path.resolve(__dirname, "src"),
          path.resolve(__dirname, "test")
        ],
        exclude: /(node_modules|bower_components)/,
        // the "loader"
        loader: "babel-loader"
      }
    ],
    node : {
        console: true,
        global: true,
        process: true,
        Buffer: true,
        __filename: true,
        __dirname: true,
        setImmediate: true
    }
  }
};
