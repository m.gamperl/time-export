select von,bis,detail,name
  from (
	select startLocalTime von ,
		   endLocalTime bis,
		   datediff(second,startLocalTime,endLocalTime)seconds,
		   null as  detail,
		   g.displayName as  Name
	  from activity a
	  join groupList as gl on gl.groupListId = a.groupListId
	  join [group] as g on g.[key] = gl.[key]
	 where a.timelineId = 1
	 union all
	select startLocalTime von ,
		   endLocalTime bis,
		   datediff(second,startLocalTime,endLocalTime)seconds,
		   a.displayName detail,
		   g.displayName Name
	  from Activity a
	  join [group] g on g.groupId = a.groupId
     ) as r
where datepart(month,von) = convert(int,'${month}')
  and datepart(year,von) = convert(int,'${year}')
  and name like '${filter}'
  and datediff(second,von,bis) >= ${minSecondsDiff}
