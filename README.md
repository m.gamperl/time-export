http://example.gitlab.com/namespace/project/badges/branch/build.svg

# time-export
Tool to export ManicTime data to .xlsx Files in DC Bank Timesheet format

## Requirements

On some systems Microsoft® SQL Server® Compact 4.0 SP1 seems to be required to use the adodb connection to the manic time database.

Install it from: https://www.microsoft.com/en-us/download/confirmation.aspx?id=30709

## Configuration

Configuration is stored in a file `config.js` at `./config/config.json`
All Configuration Parameters can be overwritten by environment variables with the following pattern: `TIME_EXPORT_<config value name/path>`
or by commandline parameters like `node src/index --<config value namne/path>=<value>`

### Common Configuration

The following configuration parameters are mandatory:

#### dbFile

The ManicTime Database to read from. It uses windows environment variables, so you can leave it untouched.

#### username
The username to write in the resulting `.xlsx`

#### locale

The locale `moment.js` and all formating is working with. Default : `de-AT`

#### outputFile

This is the file where the resulting `.xlsx` file will be written to.

#### queryParams.filter

The default query searches for all time intervals based on `queryParams.filter`, therefore this value hast to be set.

You can include all time intervals by setting it to `%`

#### queryParams.minSecondsDiff

The default query ignores time intervals smaller dann `queryParams.minSecondsDiff` therefore this value has to be set.

If you want to include all time intervals, just set the value to `0`

### Customizing the query and corresponding parameters

The Query executed against the ManicTime database is stored in `./config/query.sql`. You can freely change it but be aware that you keep the where clause conditions for `month` and `year` to get accurate data.

You can add custom queryParameters in `config.queryParams` and use them with node ES2015 template syntax in the `query.sql` file.

### Extended Configuration

You can add unlimited additional config paramters, which can all be used.

#### replaceEnvironmentVariables

Windows environment variables will be replaced in all config parameters which are listed here. Setting this value to `true` will use all config parameters, setting it to `false` will do no replacement at all.

#### replaceTimeVariables

See [replaceEnvironmentVariables].
It works the same, but replaces time tokens from moment.js. Those tokens have to be surrounded by `{{}}` to mark them for replacement. (ie `{{YYYY MM DD}}` will be `2016 01 01`)

#### replaceConfigVariables

See [replaceEnvironmentVariables]
It works the same, but replaces `${variable}` with `config.variable`.

### Merging

If you have multiple pc recording time, you can generate a merged timesheet with the following setup:

In this setup it does not matter which pc runs at first. You can even run the exports multiple times and will always have the correct timesheets.

Merging can be turned off completely by setting `mergedOutputFile` to `false`.

#### DESKTOP:
```
{
  "inputFile" : "<laptopFilePath>",
  "outputFile" : "<desktopFilePath>",
  "mergedOutputFile": "<mergedFilePath>",
  ...
}
```
#### LAPTOP:
```
{
  "inputFile" : "<desktopFilePath>",
  "outputFile" : "<laptopFilePath>",
  "mergedOutputFile": "<mergedFilePath>",
  ...
}
```
#### How it works:

DESKTOP runs the export and exports its own data to `outputFile`.

then it looks for `inputFile`:
* if it is found a merged file `mergedOutputFile` will be generated
* if not a new file containing only DESKTOP data will be generated for `mergedOutputFile`

LAPTOP will do the same thing.


## Run as Windows Task once per month

You can easily add a Windows task, which runs on the last day of every month, to periodically
export your timesheets.

Follow those simple Steps:

1) Goto: Aufgabenplanung
