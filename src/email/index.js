import * as nodemailer from "nodemailer";
import path from "path";
import fs from "fs";

export class EmailSender {

  constructor(options){
    this.options = options;

    var smtpConfig = {
      host: options.smtpServer,
      port: options.smtpPort,
      secure: options.secure // use SSL
    };

    smtpConfig.auth = options.auth ? options.auth : undefined;
    smtpConfig.tls = options.tls ? options.tls : undefined;

    this.transporter = nodemailer.createTransport(smtpConfig);
  }

  async send(){
    console.log(this.options);
    var mailOptions= {
        cc : this.options.cc,
        from: this.options.from, // sender address
        to: this.options.recipient, // list of receivers
        subject: this.options.subject, // Subject line
        text: this.options.body, // plaintext body
        html: this.options.html || this.options.body, // html body,
    };

    if(this.options.attachments){
      mailOptions.attachments = this.options.attachments.map((p)=> {
        //var name = path.
        return {path : p, filename : path.basename(p)};
      });
    }

    return new Promise((resolve,reject)=> {
    // send mail with defined transport object
    console.log(mailOptions);
      this.transporter.sendMail(mailOptions, (error, info)=>{
          if(error){
            console.error(error.message, error.stack);
            reject(error);
          }else{
            resolve(info);
          }
      });
    });
  }
}
