var _ = require("lodash");
var styles = require("./excelStyles");
var Excel = require("exceljs");

export default class ExcelExport {

  constructor(moment){
    this.styles = styles;
    this.moment = moment;
    this.offset = 4;
  }

  addHeader(sheet,options){
    sheet.addRow([options.username,"","",""]);
    sheet.addRow(["","","",""]);
    sheet.addRow(["Beginn Datum/Uhrzeit","Ende Datum/Uhrzeit","Dauer","Betreff",""]);
    sheet.addRow(["(TT.MM.JJJJ hh:mm)","(TT.MM.JJJJ hh:mm)","(autom. Ber.)",""]);

    _.merge(sheet.getCell("A1"),this.styles.largeFont);
    _.merge(sheet.getCell("A3"),this.styles.yellowBackground,this.styles.mediumBorderTop,this.styles.largeFont,this.styles.horizontalCenter);
    _.merge(sheet.getCell("A4"),this.styles.yellowBackground,this.styles.mediumBorderBottom,this.styles.smallFont,this.styles.horizontalCenter);
    _.merge(sheet.getCell("B3"),this.styles.redBackground,this.styles.mediumBorderTop,this.styles.largeFont,this.styles.horizontalCenter);
    _.merge(sheet.getCell("B4"),this.styles.redBackground,this.styles.mediumBorderBottom,this.styles.smallFont,this.styles.horizontalCenter);
    _.merge(sheet.getCell("C3"),this.styles.blueBackground,this.styles.mediumBorderTop,this.styles.largeFont,this.styles.horizontalCenter);
    _.merge(sheet.getCell("C4"),this.styles.blueBackground,this.styles.mediumBorderBottom,this.styles.smallFont,this.styles.horizontalCenter);
    _.merge(sheet.getCell("D3"),this.styles.greyBackground,this.styles.mediumBorderTop,this.styles.largeFont);
    _.merge(sheet.getCell("D4"),this.styles.greyBackground,this.styles.mediumBorderBottom,this.styles.smallFont);
  }


  addData(sheet,data,options,lastRowNr){
    var row;

    data.records.forEach((record,i)=>{
      row = i + (lastRowNr || (this.offset+1));
      //console.log("adding data at: ", row);
      sheet.addRow([this.moment(record.von).toDate(),this.moment(record.bis).toDate(),{formula : "=(B"+row+"-A"+row+")*24"},""]);

      _.merge(sheet.getCell("A"+row),this.styles.dateFormat,this.styles.largeFont);
      _.merge(sheet.getCell("B"+row),this.styles.dateFormat,this.styles.largeFont);
      _.merge(sheet.getCell("C"+row),this.styles.numberFormat,this.styles.largeFont);
    });

    return row;
  }

  addSumRow(sheet,data,options,lastRowNr) {
    var rows = lastRowNr || (data.records.length +this.offset);
    var sumRow = rows+1;

    if(!sheet.getRow(sumRow)){
      //console.log("adding sum row at: ", sumRow);
      sheet.addRow(["","",{formula : "=SUM(C5:C"+rows+")"},"S U M M E"]);
    }else{
      //console.log("setting sum row at: ", sumRow);
      var sumRowObject = sheet.getRow(sumRow);
      sumRowObject.values = ["","",{formula : "=SUM(C5:C"+rows+")"},"S U M M E"];
      sumRowObject.commit();
    }

    _.merge(sheet.getCell("A"+sumRow),this.styles.sumStyle,this.styles.boldFont);
    _.merge(sheet.getCell("B"+sumRow),this.styles.sumStyle,this.styles.boldFont);
    _.merge(sheet.getCell("C"+sumRow),this.styles.sumStyle,this.styles.numberFormat,this.styles.boldFont);
    _.merge(sheet.getCell("D"+sumRow),this.styles.sumStyle,this.styles.boldFont);
  }

  createWorkbookFromData(data,options){
    this.workbook = new Excel.Workbook();
    this.sheet = this.workbook.addWorksheet(options.sheetName);
    this.sheet.columns = [
        { width:20 },
        { width:20 },
        { width:10 },
        { width:100}
    ];

    this.addHeader(this.sheet,options);
    this.addData(this.sheet,data,options);
    this.addSumRow(this.sheet,data,options);

    return  this.workbook;
  }

  async writeWorkbook(workbook,destinationFile){
    try {
      await workbook.xlsx.writeFile(destinationFile)
    }catch(e){
      console.error(e);
    }
  }

  async readWorkbook(sourceFile){
    try {
      this.workbook = new Excel.Workbook();
      await this.workbook.xlsx.readFile(sourceFile);
      return this.workbook;
    }catch(e){
      console.error(e);
    }
  }

  addNewDataToSheet(sheet,data,options){
    var row = this.offset;
    var cell = {};
    while(true){
      //console.log("check row: ", row, sheet.getCell("C"+row));
      cell = sheet.getCell("D"+row);
      if(cell.value === "S U M M E"){
        break;
      }
      row++;
    }
    //console.log("sum row at: ", row);
    var rowObject = sheet.getRow(row)
    rowObject.values = [];
    rowObject.commit();

    //console.log("found sum row at: ", row);

    var lastRow = this.addData(sheet,data,options,row);
    this.addSumRow(sheet,data,options,lastRow);

  }

}
