var _ = require("lodash");
var red = { argb : "FFFF0000"};
var yellow = { argb : "FFFFCC00"};
var green = { argb : "FF00FF00"};
var blue = {argb : "FF99CCFF"};
var grey = {argb : "FFC0C0C0"};
var borderColor = {argb : "FF000000"};

function background(color){
  return {type : "pattern",
    pattern : "solid",
    fgColor : color
  }
}

function fontStyle(name,size,horizontalAlign,verticalAlign,bold){
  var result = _.clone({});

  if(bold !== undefined){
    if(!result.font)
      result.font = {};

    result.font.bold = bold;
    //_.merge(result,{font : {bold : bold}}));
  }

  if(name){
    _.merge(result,{font : {name : name }});
  }

  if(size){
    _.merge(result,{font : {size : size }});
  }

  if(horizontalAlign){
    _.merge(result,{alignment : {horizontal : horizontalAlign }});
  }

  if(verticalAlign){
    _.merge(result,{alignment : {vertical : verticalAlign }});
  }

  return _.cloneDeep(result);
}

function border(top,right,bottom,left){
  var border = {};
  if(top){
    _.merge(border,{top: {style:top, color: {argb:borderColor}}})
  }
  if(right){
    _.merge(border,{right: {style:right, color: {argb:borderColor}}})
  }
  if(bottom){
    _.merge(border,{bottom: {style:bottom, color: {argb:borderColor}}})
  }
  if(left){
    _.merge(border,{left: {style:left, color: {argb:borderColor}}})
  }
  return border;
}

module.exports = {
  defaultColumnStyle : _.cloneDeep({font : {name : "Arial", size : 10}}),
  sumStyle : {
    border : border("medium",null,"medium",null),
    fill : background(green)
  },
  dateFormat : {style : {numFmt: "dd/mm/yyyy hh:mm:ss"}},
  numberFormat : {style : {numFmt: "0.00"}},
  smallFont : fontStyle("Arial",8,null,"middle"),
  largeFont : fontStyle("Arial",10,null,"middle"),
  boldFont : {
    font : {
      bold : true
    }
  },
  horizontalLeft : { alignment : {horizontal : "left"}},
  horizontalCenter : fontStyle(null,null,"center",null),
  yellowBackground : {fill : background(yellow)},
  redBackground: {fill : background(red)},
  blueBackground : {fill : background(blue)},
  greyBackground : {fill : background(grey)},
  mediumBorder : {border : border("medium","medium","medium","medium")},
  mediumBorderTop : {border : border("medium")},
  mediumBorderBottom : {border : border(null,null,"medium")},
  background :background,
  border : border,
  fontStyle : fontStyle
}
