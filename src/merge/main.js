require("babel-polyfill");

var moment = require("moment");
var path = require("path");
var _ = require("lodash");
import ExcelExport from "../excel";
import Db from "../db";
import Config from "../config";

/*
parameters:
  - locale,
  - username,
*/
async function main(){

  var config = new Config(moment);
  var options = config.getOptions({});

  var excelExport = new ExcelExport(moment);

  try {

    var workbook = await excelExport.readWorkbook(options.outputFile);

    var data = {records : [{von : new Date(), bis : new Date()}]};

    var result = excelExport.addNewDataToSheet(workbook.getWorksheet(1),data,options);

    await excelExport.writeWorkbook(workbook,"C:/Users/Mathias/Google Drive/Arbeitszeiten/new.xlsx");
    //console.log(result, workbook.getWorksheet(1));

  }catch(e){
    console.error("Error: ", e.stack);
  }
}

main();

process.on("unhandledRejection",(e)=>{
  console.error(`Error: ${e.stack}`, e);
});

process.on("uncaughtException",(reason,p)=>{
  console.error(`Error: ${reason.stack} - ${p}`, p.stack);
});
