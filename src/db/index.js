var ADODB = require('node-adodb');
var path = require("path");
var _ = require("lodash");
var fs = require("fs");

ADODB.debug = false;
//" %LOCALAPPDATA%/finkit/manicTime/ManicTime.sdf",

export default class Db {

  constructor(dataFile){
    this.query = fs.readFileSync("./config/query.sql");
    console.log("open connection for ", dataFile);
    this.connection = ADODB.open('Provider=Microsoft.SQLSERVER.CE.OLEDB.4.0;Data Source='+ dataFile +';');
    //console.log("opened connection to ", dataFile, this.connection, this.query);
  }

  getQuery(params){
    try {
      params.filter = params.filter || "%";
      var result = _.template(this.query)(params);
      return result;
    }catch(e){
      console.log(e.stack);
      throw e;
    }
  }

  async queryData(options){
    return new Promise((resolve,reject)=>{
      try {
        this.connection
          .query(this.getQuery(options))
          .on('done', (data)=>{
            resolve(data);
          })
          .on('fail', (error)=>{
            console.error(error);
            reject(error);
          });
      }catch(e){
        reject(e);
      }
    });
  }

}
