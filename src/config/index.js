var fs = require("fs");
var path = require("path");
var _ = require("lodash");

export default class Config {

  constructor(moment){
    this.moment = moment;
  }

  replaceEnvironmentVariables(options,keys,ignores){
    return this.replaceVariables(options,keys,ignores,(value)=> {
      var ret = value.replace(/%([^%]+)%/g, (_,n)=> {
          return process.env[n]
      });
      return path.normalize(ret);
    },(v)=>v);
  }

  replaceTimeVariables(options,keys,ignores,month,year){
    return this.replaceVariables(options,keys,ignores,(value)=> {
      //console.log("value=",value);
      return value.replace(/{{([^}}]+)}}/g, (_,n)=> {
        return this.moment("01."+month+"."+year,"DD.MM.YYYY").format(n);
      })
    },(v)=>v);
  }

  replaceConfigVariables(options,keys,ignores){
    return this.replaceVariables(options,keys,ignores,(value)=> {
      return _.template(value)(options);
    });
  }

  replaceVariables(options,keys,ignores,func){
    if(keys !== undefined){

      if(keys === true){
        keys = _.keys(options);
      }

      var filtered = keys;

      if(ignores){
        filtered = _.filter(keys,ignores);
      }

      filtered.forEach((key)=>{
        if(_.isString(_.get(options,key))){
          _.set(options,key,func(_.get(options,key)));
        }

        if(_.isArray(_.get(options,key))){
          var arr = _.get(options,key);
          //console.log("set: ", key);
          _.set(options,key,arr.map((v)=>{
            //console.log("v: ", func(v));
            return func(v);
          }));
        }
      });
    }
    return options;
  }

  getOptions(params,addMonths){

    var env = _(process.env)
      .pickBy((v,k)=>{
      return k.indexOf("TIME_EXPORT_") === 0;
    }).mapKeys((v,k)=>{
      return k.replace("TIME_EXPORT_","");
    }).value();

    var config = JSON.parse(fs.readFileSync("./config/config.json"));
    //var config = require("../../config/config.json");
    this.moment.locale(config.locale);

    var month = _.get(params,"queryParams.month") || params.month || this.moment().month()+1+addMonths;
    var year = _.get(params,"queryParams.year") || params.year || this.moment().year();

    var options = _.merge(config,env,params,{queryParams : { month, year}});

    options = this.replaceVariables(options,options.readFiles,false,(v)=>{
      return fs.readFileSync(v,"utf-8");
    });
    //console.log("month=",month,"year=",year,params);
    //console.log("options: ", options);
    options = this.replaceEnvironmentVariables(options,options.replaceEnvironmentVariables);
    options = this.replaceTimeVariables(options,options.replaceTimeVariables,false,month,year);
    options = this.replaceConfigVariables(options,options.replaceConfigVariables);

    this.replaceEnvironmentVariables(options.queryParams,true,options.ignoreQueryParamsReplacement);
    this.replaceTimeVariables(options.queryParams,true,options.ignoreQueryParamsReplacement,month,year);
    this.replaceConfigVariables(options.queryParams,true,options.ignoreQueryParamsReplacement);

    //console.log("options=",options);
    return options;
  }

}
