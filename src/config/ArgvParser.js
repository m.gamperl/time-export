import _ from "lodash";

export default class ArgvParser{

  constructor(argv){
    this.argv = argv;
  }

  getParamObject(){
    return this.parseParameters();
  }

  parseParameters(){
    var result= {};
    return this.argv.reduce((s,v)=>{
      if(v.indexOf("=") > 0){
        var splitted = v.split("=");
        _.set(s,splitted[0],splitted[1]);
        //s[splitted[0]] = splitted[1];
      }
      return s;
    },{});
  }

}
