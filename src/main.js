require("babel-polyfill");
var moment = require("moment");
var path = require("path");
var _ = require("lodash");
var fs = require("fs");
import ExcelExport from "./excel";
import Db from "./db";
import Config from "./config";
var mkdirp = require('mkdirp');
import ArgvParser from "./config/ArgvParser";
import {EmailSender} from "./email";

/*
parameters:
  - locale,
  - username,

*/
async function main(){

  var argvParser = new ArgvParser(process.argv);

  var config  = new Config(moment);
  var options = config.getOptions(argvParser.getParamObject(),0);

  //console.log(options);

  if(options.sendEmail){
    options = config.getOptions(argvParser.getParamObject(),-1);
    console.log("sending Email");
    var emailSender = new EmailSender(options.email);
    try {
      await emailSender.send();
    }catch(e){
      console.error(e);
    }
  }else{
    var excelExport = new ExcelExport(moment);
    //console.log("opening database: ", options.dbFile);
    var db = new Db(options.dbFile);

    try {
      //console.log("queryParams",options.queryParams);
      //console.log("querying databse: ", options.queryParams);
      var data = await db.queryData(options.queryParams);
      //console.log("data received: ", data);
      var workbook;
      var mergedWorkbook;
      var mergedOutputFileOutstanding = true;

      if(options.inputFile !== undefined){

        //if possible read inputFile and write merged
        try {
          console.log("checking for inputFile: ", options.inputFile);
          fs.accessSync(options.inputFile,fs.OK_R);

          console.log("Merging with inputFile: ", options.inputFile);
          mergedWorkbook = await excelExport.readWorkbook(options.inputFile);
          excelExport.addNewDataToSheet(mergedWorkbook.getWorksheet(1),data,options);

          console.log("Writing merged workbook to: ", options.mergedOutputFile);
          mkdirp.sync(path.dirname(options.mergedOutputFile));
          await excelExport.writeWorkbook(mergedWorkbook,options.mergedOutputFile);
          mergedOutputFileOutstanding = false;
        } catch(e){
          //console.error(e);
        }

      }

      //write my output file
      if(options.outputFile !== undefined){
        mkdirp.sync(path.dirname(options.outputFile));
        console.log("Creating new workbook: ", options.outputFile);
        workbook = excelExport.createWorkbookFromData(data,options);
        console.log("Writing workbook to: ", options.outputFile);
        await excelExport.writeWorkbook(workbook,options.outputFile);
      }

      //write merged output file if not yet written
      if(mergedOutputFileOutstanding && options.mergedOutputFile){
        mkdirp.sync(path.dirname(options.mergedOutputFile));
        console.log("Writing workbook to: ", options.mergedOutputFile);
        await excelExport.writeWorkbook(workbook,options.mergedOutputFile);
      }

    }catch(e){
      console.error("Error: ", e.stack);
    }
  }
}

process.on("unhandledRejection",(e)=>{
  console.error(`Error: ${e.stack}`, e);
});

process.on("uncaughtException",(reason,p)=>{
  console.error(`Error: ${reason.stack} - ${p}`, p ? p.stack : p);
});


module.exports = {
  main : main
}
